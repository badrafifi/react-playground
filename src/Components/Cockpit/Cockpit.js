import React from 'react';
import Classes from './Cockpit.css'

const cockpit = (props) => {
    const pClass = [];

    let buttonClass = '';


    // Style conditionning
    if (props.persons.length <= 2) {
        pClass.push(Classes.red)
    }
    if (props.persons.length <= 1) {
        pClass.push(Classes.bold)
    }
    if (props.showPersonList) {
        buttonClass = Classes.Red;
    }
    return (
        <React.Fragment>
            <header className={Classes["Cockpit-header"]}>
                <h1 className={Classes["Cockpit-title"]}>Welcome to React</h1>
                <h2>{props.title}</h2>
            </header>
            <p className={pClass.join(' ')}>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem dolore ducimus iste
                nobis placeat tempora veritatis? Aliquid atque, autem deserunt, id labore magnam molestiae nisi,
                officia similique velit voluptate?
            </p>
            <button onClick={props.buttonClicked} className={buttonClass}>Show/Hide</button>
        </React.Fragment>
    )
}
export default cockpit;