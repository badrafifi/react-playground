import React, {Component} from 'react';
import Person from "./Person/Person";

class Persons extends Component {
    shouldComponentUpdate(nextProps,nextState,nextContext){
        console.log(nextProps.personsArray);
        console.log(this.props.personsArray);
        // only update if persons are different
        return this.props.personsArray !== nextProps.personsArray;
    }
    render() {
        return this.props.personsArray.map((per, index) => {
            return (
                <Person
                    key={per._id}
                    name={per.name}
                    age={per.age}
                    personClicked={() => this.props.clicked(index)}
                    changeName={(event) => {
                        this.props.changed(event, index)
                    }}
                />);
        });
    }
}
// // statless component persons listing :
//  const persons = (props) => props.personsArray.map((per, index) => {
//     // return < Person name={per.name} age={per.age} personClicked={this.deletePersonHandler(index).bind(this)}/>
//     return (
//         <Person
//             key={per._id}
//             name={per.name}
//             age={per.age}
//             personClicked={() => props.clicked(index)}
//             changeName={(event) => {
//                 props.changed(event, index)
//             }}
//         />);
// });
export default Persons;