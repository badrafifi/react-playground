import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Classes from './Person.css'

export class Person extends Component {
    constructor(props){
        super(props);
        this.inputElementRef = React.createRef();
    }
    componentDidMount() {
        if (this.props.position === 0) {
            this.inputElementRef.current.focus();
        }
    }

    render() {
        return (

            <div className={Classes.Person}>
                <p onClick={this.props.personClicked}>I'm {this.props.name} and i'm {this.props.age} years
                    old {this.props.children ? ',' : ''} {this.props.children}</p>
                <input
                    ref={this.inputElementRef}
                    type='text' onChange={this.props.changeName} value={this.props.name}/>
            </div>
        )
    }

}

// const person = (props) => {
//     // const rand = Math.random();
//     // if (rand > 0.7) {
//     //     throw new Error('Something went wrong')
//     // }
//     // else
//     return (
//
//         <div className={Classes.Person}>
//             <p onClick={props.personClicked}>I'm {props.name} and i'm {props.age} years
//                 old {props.children ? ',' : ''} {props.children}</p>
//             <input type='text' onChange={props.changeName} value={props.name}/>
//         </div>
//     )
// };
Person.propTypes = {
    personClicked: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    changeName: PropTypes.func,
}
export default Person;