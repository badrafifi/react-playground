import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './Containers/App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App mainTitle="My React App" />, document.getElementById('root'));
registerServiceWorker();
