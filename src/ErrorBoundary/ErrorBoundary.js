import React, {Component} from 'react';

export class ErrorBoundary extends Component {
    state = {
        hasError: false,
        errorMessage: ''
    };
    componentDidCatch = (err, info) => {
        this.setState({hasError: true, errorMessage: err});
    };

    render() {
        if (this.state.hasError) {
            return (<h1>{this.state.errorMessage}</h1>)
        }
        //
        else {
            console.log(this.props.children);
            return this.props.children;
        }
    }
}