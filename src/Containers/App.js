import React, {Component} from 'react';
import './App.css';
import Persons from '../Components/Persons/Persons';
import Cockpit from '../Components/Cockpit/Cockpit';
import Classes from './App.css'

class App extends Component {
    // App implements a container behavior , which means ,
    // it manages and handles the state ,
    // passes results to components

    //update cch
    constructor(props) {
        super(props);
        console.log('App.js constructor called');
    }

    // componentWill
    componentWillMount() {
        console.log('App.js componentWillMount called');
    }

    componentDidMount() {
        console.log('App.js componentDidMount called');
    }

    //update lch
    shouldComponentUpdate() {
        console.log('App.js shouldComponentUpdate called');
        return true
    }

    componentWillUpdate(nextProps,nextState,nextContext) {
        console.log('App.js componentWillUpdate called');
    }
    componentDidUpdate() {
        console.log('App.js componentDidUpdate called');
    }


    state = {
        persons: [
            {_id: '498', name: 'eazeazea', age: 78},
            {_id: '321', name: 'fze f', age: 21},
            {_id: '987', name: 'zerfezsqzd', age: 32},
        ],
        showPersons: false,
        toggleClicked: 0
    };
    changeNameHandler = (event, index) => {
        console.log(event.target.value);
        const newPersonsList = [...this.state.persons];
        newPersonsList[index]['name'] = event.target.value;
        this.setState({persons: newPersonsList})
    };
    deletePersonHandler = (personIndex) => {
        // Bad practice : this.state.persons.splice(index, 1);
        const newPersonsList = [...this.state.persons];
        newPersonsList.splice(personIndex, 1);
        this.setState({persons: newPersonsList});
    };
    togglePersonsHandler = () => {
        this.setState((prevState, props) => {
            return {
                showPersons: !prevState.showPersons,
                // prevent danger of changing state anywhere else in the app
                // set state is an async call
                toggleClicked: prevState.toggleClicked +1,
            }
        });
    };


    render() {
        console.log('App.js render called');

        //button style
        // gets executed in each state change
        let persons = null;
        //ng if
        if (this.state.showPersons) {
            //ng for
            persons = (
                <div>
                    <Persons
                        personsArray={this.state.persons}
                        clicked={this.deletePersonHandler}
                        changed={this.changeNameHandler}
                    >
                    </Persons>
                </div>
            );
        }


        return (
            <div className={Classes.App}>
                <Cockpit
                    title={this.props.mainTitle}
                    persons={this.state.persons}
                    showPersonList={this.state.showPersons}
                    buttonClicked={this.togglePersonsHandler}
                />
                {persons}
            </div>
        );
        // return React.createElement('div', {className:'App'}, React.createElement('h1', null, 'Test React'));
    }
}

export default App;
